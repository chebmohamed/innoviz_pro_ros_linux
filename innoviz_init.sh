#! /bin/bash
source /opt/ros/melodic/setup.sh
echo "Ros environement sourcing complete"
cd ~/catkin_ws
catkin_make
echo "Building complete"
source ./devel/setup.sh
echo "Devel sourcing complete"
echo "Connecting to sensor ...."
cd src/innoviz_ros/launch
roslaunch innoviz_ros.launch